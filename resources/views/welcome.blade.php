<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>bensutter</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="css/app.css" />
        <script src="https://use.fontawesome.com/26f906e461.js"></script>

    </head>
    <body>
        <div id="inner"></div>
            <div class="center">
                <p>Coming soon</p>
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </body>
</html>
